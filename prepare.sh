#!/bin/sh

wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1442O7SjHJY6Twzh0pxyYqCN-uNbVLyTr' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1442O7SjHJY6Twzh0pxyYqCN-uNbVLyTr" -O checkpoint.zip && rm -rf /tmp/cookies.txt
unzip checkpoint.zip -d to_mount
rm checkpoint.zip

mkdir to_mount/models
mkdir to_mount/samples
mkdir to_mount/store
