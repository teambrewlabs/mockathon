# Setup

In this repo, run:

```sh
poetry install
poetry run uvicorn mockathon:app --reload
```

The web API docs will be served at http://127.0.0.1:8000
