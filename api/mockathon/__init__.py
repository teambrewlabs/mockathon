from fastapi import FastAPI, BackgroundTasks
from fastapi.concurrency import run_in_threadpool
from fastapi.staticfiles import StaticFiles
from pydantic import BaseModel, BaseSettings
import httpx
from typing import List, Optional
from random import choice
from uuid import UUID, uuid4
import zmq
import json
from pathlib import Path

from mockathon.team import generate_team, InputTeam, Team, image_lookup
from mockathon.project import ProblemDomain, TechDomain, generate_project


class Settings(BaseSettings):
    pixabay_api_key: str
    gpt2_server_address: str    # TODO: validator
    filesystem_store: str      # TODO: validator

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"


class Project(BaseModel):
    name: str
    tagline: str
    inspiration: Optional[str]
    what_does: Optional[str]
    how_built: Optional[str]
    challenges: Optional[str]
    accomplishments: Optional[str]
    learned: Optional[str]
    whats_next: Optional[str]
    built_with: Optional[List[str]]

class SlideData(BaseModel):
    background_image_url: str

class InputProject(BaseModel):
    problem_domain: Optional[ProblemDomain]
    tech_domain: Optional[TechDomain]
    team: InputTeam


class ProjectResponse(BaseModel):
    project: Project
    team: Team
    slides: SlideData


# TODO: rename
class UUIDResponse(BaseModel):
    uuid: str


settings = Settings(_env_file="settings.env", _env_file_encoding="utf=8")
app = FastAPI()

# Initialise store
store_path = Path(settings.filesystem_store)
store_path.mkdir(exist_ok=True)

context = zmq.Context()
print("Connecting to GPT-2 server at {}...".format(settings.gpt2_server_address))
socket = context.socket(zmq.REQ)
socket.connect(settings.gpt2_server_address)

async def process(project_id: str, input: InputProject) -> ProjectResponse:
    team_name, project_title, tagline = generate_project(input.problem_domain, input.tech_domain)
    req = dict(inspiration=tagline)
    print(f"Sending request {req} ...")
    socket.send_string(json.dumps(req))

    # This may fail if the server returns the empty string
    # In this case, the background process will restart correctly (I hope)
    msg = json.loads(socket.recv_string())
    print(f"Received response {msg} ...")

    project = ProjectResponse(
        project = {
            "name": project_title,
            "tagline": tagline,
            **msg
        },
        team = await generate_team(settings.pixabay_api_key, input.team, team_name),
        slides = {
            # TODO: non-person fallback
            "background_image_url": await image_lookup(settings.pixabay_api_key, "landscapey"),
        }
    )

    print(project)

    with open(store_path / project_id, 'w') as f:
        json.dump(project.dict(), f)




@app.post("/submit_project", response_model=UUIDResponse)
async def submit_project(input: InputProject, background_tasks: BackgroundTasks):
    project_id = str(uuid4()) + ".json"
    background_tasks.add_task(process, project_id, input)
    return UUIDResponse(uuid=project_id)


@app.post("/simple_generate", response_model=ProjectResponse)
async def simple_generate(input: InputProject):
    team_name, project_title, tagline = ("Test Team", "Test Project", "Project taglineeeeeee")
    return {
        "project": {
            "name": project_title,
            "tagline": tagline,
            "inspiration": "Test inspiration",
            "what_does": "What it does What it does What it does What it does What it does. What it does What it does What it does What it does What it does. What it does What it does",
            "how_built": "How it's built How it's built How it's built How it's built How it's built How it's built. How it's built How it's built How it's built How it's built How it's built How it's built.",
            "challenges": "Challenges Challenges Challenges Challenges Challenges Challenges. Challenges Challenges Challenges Challenges Challenges Challenges Challenges Challenges Challenges.",
            "accomplishments": "Accomplishments AccomplishmentsAccomplishmentsAccomplishmentsAccomplishmentsAccomplishments. Accomplishments Accomplishments Accomplishments.",
            "learned": "learnings learnings learnings learnings learnings. learnings learnings learnings learnings learnings.",
            "whats_next": "",
            "built_with": ["FastAPI", "Sellotape"],
        },
        "team": await generate_team(settings.pixabay_api_key, input.team, team_name),
        "slides": {
            "background_image_url": await image_lookup(settings.pixabay_api_key, "landscape"),
        }
    }


app.mount("/store", StaticFiles(directory=settings.filesystem_store), name="store")
app.mount("/", StaticFiles(directory="site", html=True), name="static")
