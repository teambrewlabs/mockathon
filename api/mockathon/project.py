import random
from typing import Optional

from pydantic import BaseModel
from typing import Optional
from enum import Enum, auto


class ProblemDomain(Enum):
    climate_change = "climate_change"
    transport = "transport"
    banking = "banking"
    education = "education"
    hackathons = "hackathons"
    health = "health"

class TechDomain(Enum):
    ai = "ai"
    crypto = "crypto"
    mobile_apps = "mobile_apps"
    cloud = "cloud"
    vr = "vr"
    drones = "drones"
    web3 = "web3"


def generate_team_name():

    adjective_list=["Green", "Red", "Blue", "Orange", "Fungible", "Crypto", "Cryptographic", "Python", "C++", "NFT",
                    "Black Hat", "White Hat", "Grey Hat", "Java", "Javascript", "Ecological", "Socially Conscious",
                   "Speculatory", "All-night", "Overwhelmed", "Disaster"]
    noun_list=["NFT", "Coder", "Hacker", "Tabber", "Compiler", "Token", "Engineer", "Caffeinator", "Constructor", "Failure",
              "CEO", "Bodger", "Jobber", "Intern", "Wheezer", "Java User", "Warrior", "Gamer", "Roller", "Disaster", "Champion",
              "Astronaut"]
    adjective=random.choice(adjective_list)
    noun=random.choice(noun_list)

    return ("%s %ss" %(adjective, noun))

def generate_project_title(technology, issue):
    if issue is None:
        issue=random.choice(["Climate Change", "Transport", "Banking", "Eduction", "Hackathon", "Health", "Memes"])
    else:
        issue = issue.value

    if technology is None:
        technology = random.choice(["AI", "Web3.0", "Machine Learning", "Blockchain / Crypto"])
    else:
        technology = technology.value

    solve=random.choice(["solve", "change", "revolutionise", "disrupt", "overthrow", "fix"])

    #Buzzword = random.choice(["Synergy", "Alpha", "Apollo", "Athena", "Atomic", "Eclipse",
    #           "Endeavour", "Evolve", "Forte", "Hydra", "Ignite", "Instinct",
    #           "Valor", "Mystic", "Logic", "Phoenix", "Platnium", "Revolution", "Radical"
    #           "Stealth", "Velocity", "Venture", "Paradigm", "Proactive", "Nexus", "Dynamism", "Innovation",
    #                         "Trailblaze"])

    return ("'Using %s to %s %s!'" %(technology, solve, issue))


def madlibs_sub(variable, team_name="The Fungible Tokens", project_title="Using AI to Roll down in the Deep"):

    number_list=range(1,101)
    number_list = [str(i) for i in number_list]


    wordlist = {"personal_verb":["create", "build", "imagine", "fix", "provide", "catalyse", "cause", "effect", "generate",
                            "invoke", "produce", "bring forth","repair", "renovate", "work on"],
            "abstract_noun":["realities", "dreams", "hackathon project ideas", "futures", "presents",
                           "alternative realities", "tomorrows", "metaverses", "crypto miners", "stocks", "finances",
                            "NFTs"],
            "resource":["time", "money", "energy", "electricity", "laptops", "GPUs", "intern labour", "bitcoin mining rigs",
                      "machine learning algorithms", "a good programming language", "free Jane Street T-Shirts"],
            "relation":["family", "friends", "colleagues", "partner", "ex-husband", "ex-wife", "Tinder date",
                        "Grindr date", "partners", "friend with benefits", "fiance", "future wife"],
            "number":number_list,
            "subject":["computer science", "physics", "mathematics", "photography", "clown studies", "business",
                      "MBA", "engineering"],

            "graduates":["graduates", "undergradutes", "prospectives", "drop outs", "rejects"],

            "profession":["programmer", "professional", "researcher", "developer", "student", "hobbyist",
                          "corn exchanger", "trader", "stock exchange", "NFT minter", "crypto miner",
                          "Doctor", "[language] developer"],
            "conjunction":["Unfortunately", "However", "Luckily", "Thankfully", "Amazingly"],

            "language":["Python", "Java", "C", "C+", "C++", "C-", "Matlab", "R", "Perl", "Fortran", "Haskell",
                       "C#", "Oberon", "Machine Code", "Wasam", "Algol", "Cobal", "Brainfudge"],

            "change":["change", "fix", "stop", "prevent", "put a stop to"],

                "addition":["combine", "connect", "link", "integrate", "unite"],

            "project_name":[project_title],

            "team_name":[team_name],

            "declerative_noun":["thing", "solution"],
            "funny_noun":[""],
            "verb":["roll"],
            "person":["Me"]

           }

    while "[" in variable:
        #random.seed()
        x1=(variable.index("["))
        x2=(variable.index("]"))
        key=(variable[x1+1:x2])
        replacement = variable[x1:x2+1]
        #print(key)
        variable = variable.replace(replacement, random.choice(wordlist[key]), 1)


    return(variable)

TeamName = str
ProjectTitle = str
Tagline = str

def generate_project(problem_domain: Optional[ProblemDomain], tech_domain: Optional[TechDomain]) -> \
        (TeamName, ProjectTitle, Tagline):


    problem_list = ["Have you ever wanted to [personal_verb] all sorts of [abstract_noun], with only limited access to [resource]?",
                "Have you and your [relation] ever experienced problems with the way you [personal_verb] your [abstract_noun]?",
               "[number]% of [subject] [graduates] would like to work as a [profession]. [conjunction], [number]% actually end up as [profession]s.  Clearly, a solution is required.",
               "Everyone knows [profession]s have a bad time. We want to [change] this.",
                    "On the one hand, [abstract_noun]. On the other hand, [profession]s. How can we [addition] these?"
               ]

    solution_list = ["Introducing [project_name] By the [team_name]!",
                     "Well, you need [project_name] By the [team_name]!",
                    "Have I got the [declerative_noun] for you: [project_name] By the [team_name]!",
                     "We're here to solve all of your problems with [project_name] By the [team_name]!",
                    "We here at [team_name] have synergised a solution to this conundrum: [project_name]"]



    team_name=generate_team_name()
    project_title=generate_project_title(tech_domain, problem_domain)

    problem = random.choice(problem_list)
    solution = random.choice(solution_list)

    problem = madlibs_sub(problem, team_name, project_title)
    solution = madlibs_sub(solution, team_name, project_title)

    tagline=problem+" "+solution

    return(team_name, project_title, tagline)

##Pass ProblemDomain and TechDomain here##
team_name, project_title, tagline = generate_project(ProblemDomain.climate_change, TechDomain.vr)
