from pydantic import BaseModel
from typing import List
import random
import httpx
from urllib.parse import quote_plus
import json


# Returns image URL
async def image_lookup(API_KEY: str, query: str) -> str:
    query_safe = quote_plus(query)
    async with httpx.AsyncClient() as client:
        try:
            r = await client.get(f"https://pixabay.com/api/?key={API_KEY}&q={query_safe}")
            return random.choice(json.loads(r.text)["hits"])["largeImageURL"]
        except IndexError:
            r = await client.get(f"https://pixabay.com/api/?key={API_KEY}&q=avatar")
            return random.choice(json.loads(r.text)["hits"])["largeImageURL"]


class InputMember(BaseModel):
    first_name: str
    last_name: str

class InputTeam(BaseModel):
    members: List[InputMember]

class Member(BaseModel):
    first_name: str
    last_name: str
    nickname: str
    role: str
    image_url: str

class Team(BaseModel):
    name: str
    members: List[Member]

ROLE_1 = ["Sussy", "Chief", "Professional", "Amateur", "The", "Motivational", "Part-time", "Full-time", "Meticulous", "Miraculous", "Unintentional", "Intentional"]
ROLE_2 = ["Caffeinator", "Shoplifter", "Ikea Workbench Constructor", "Business", "Baka", "Failure", "Faffer", "Micromanager", "Backseat driver", "Worker", "Distractor", "Pen pusher", "Funger", "Creative"]
NICKNAMES = ["Shark", "Slick", "HTML", "Full stack", "Memelord", "Agile", "Big Data", "Fungible", "Blockchain", "Satoshi", "Synergy", "New normal", "DevOps", "SysOps", "Pull request", "Git", "Hash Table", "rm -rf / --no-preserve-root", "Segfault", "Leverage", "Customer Impact", "Content Creator", "Bankruptcy", "Buzzword", "Reimagine", "Paradigm", "API", "SaaS", "Spamlord", "Lily Gilder","The Rock"]

async def generate_team_member(API_KEY: str, member: InputMember) -> Member:
    role = random.choice(ROLE_1) + " " + random.choice(ROLE_2)
    return Member(
        first_name = member.first_name,
        last_name = member.last_name,
        nickname = random.choice(NICKNAMES),
        role = role,
        image_url = await image_lookup(API_KEY, role.split(' ')[-1])
    )

async def generate_team(API_KEY: str, team: InputTeam, team_name: str) -> Team:
    members = []
    for member in team.members:
        members.append(await generate_team_member(API_KEY, member))

    return Team(
        name = team_name,
        members = members
    )
