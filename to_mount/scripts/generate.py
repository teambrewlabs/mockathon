#!/usr/bin/env python3

import gpt_2_simple as gpt2
import os
import requests
import zmq
import json

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5555")

def to_dict(chunks):
    chunks = [ c.strip() for c in chunks ]
    discard_index = chunks.index("")
    chunks = chunks[:discard_index]

    while "GitHub Repo" in chunks:
        chunks.remove("GitHub Repo")
    while "Try it out" in chunks:
        chunks.remove("Try it out")

    output = dict()

    indices_names = [
        "inspiration",
        "what_does",
        "how_built",
        "challenges",
        "accomplishments",
        "learned",
        "whats_next",
        "built_with",
        "name"
    ]

    indices_lookup = [
        ("Inspiration", "inspiration"),
        ("What it does", "what_does"),
        ("How I built it", "how_built"),
        ("How we built it", "how_built"),
        ("Challenges I ran into", "challenges"),
        ("Challenges we ran into", "challenges"),
        ("Accomplishments that I'm proud of", "accomplishments"),
        ("Accomplishments that we're proud of", "accomplishments"),
        ("What I learned", "learned"),
        ("What we learned", "learned"),
        (None, "whats_next"),
        ("Built With", "built_with"),
    ]
    indices = []

    for (l, i) in indices_lookup:
        if l is None:
            try:
                ind = [ i for i, x in enumerate(chunks) if x.startswith("What's next for") ][0]
                indices.append((i, ind))
                output['name'] = chunks[ind][16:]
            except IndexError:
                pass
        else:
            try:
                indices.append((i, chunks.index(l)))
            except ValueError:
                pass

    indices = [ (ind, name) for name, ind in indices ]
    indices.sort()
    indices.append((len(chunks), None))

    for i, (ind, name) in enumerate(indices):
        if name is not None:
            ind2 = indices[i+1][0]
        else:
            continue

        chunks_sliced = chunks[ind+1:ind2]

        if name != "built_with":
            chunks_sliced = '\n'.join(chunks_sliced)

        output[name] = chunks_sliced

    for n in indices_names:
        if output.get(n) is None:
            output[n] = None

    return output


model_name = "355M" #"124M"
if not os.path.isdir(os.path.join("models", model_name)):
    print("Downloading {} model...".format(model_name))
    gpt2.download_gpt2(model_name=model_name)   # model is saved into current directory under /models/124M/


sess = gpt2.start_tf_sess()
gpt2.load_gpt2(sess)

def run_model(prefix):
    print("Running model...")
    output_dict = None

    while output_dict is None:
        output = gpt2.generate(sess,
            length=1000,
            #temperature=0.7,
            prefix=prefix,
            #nsamples=5,
            #batch_size=5,
            return_as_list=True,
        )

        print("Output generated! Parsing...")

        output_list = output[0].split("\n")

        try:
            output_dict = to_dict(output_list)
        except:
            print("Parsing error, trying again")
            pass
    return output_dict

while True:
    print("Waiting for message...")

    try:
        message = json.loads(socket.recv_string())
    except:
        print("Error receiving message :(")
        socket.send_string("")
        continue

    print("Received request: {}".format(message))

    try:
        inspiration = message.get("inspiration")
    except:
        socket.send_string("")
        continue

    prefix = "Inspiration\n{}".format(inspiration)
    name = None
    try:
        while name is None:
            result = run_model(prefix)
            name = result.get("name")
            if name is None:
                print("No name generated! Trying again...")
    except:
        socket.send_string("")
        continue

    print("Result: {}".format(result))
    try:
        to_send = json.dumps(result)
    except:
        socket.send_string("")
        continue
    socket.send_string(to_send)
