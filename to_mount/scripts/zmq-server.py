#!/usr/bin/env python3

import zmq
import json

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5555")

def to_dict(chunks):
    chunks = [ c.strip() for c in chunks ]
    discard_index = chunks.index("")
    chunks = chunks[:discard_index]

    while "GitHub Repo" in chunks:
        chunks.remove("GitHub Repo")
    while "Try it out" in chunks:
        chunks.remove("Try it out")

    output = dict()

    indices_names = [
        "inspiration",
        "what_does",
        "how_built",
        "challenges",
        "accomplishments",
        "learned",
        "whats_next",
        "built_with",
        "name"
    ]

    indices_lookup = [
        ("Inspiration", "inspiration"),
        ("What it does", "what_does"),
        ("How I built it", "how_built"),
        ("How we built it", "how_built"),
        ("Challenges I ran into", "challenges"),
        ("Challenges we ran into", "challenges"),
        ("Accomplishments that I'm proud of", "accomplishments"),
        ("Accomplishments that we're proud of", "accomplishments"),
        ("What I learned", "learned"),
        ("What we learned", "learned"),
        (None, "whats_next"),
        ("Built With", "built_with"),
    ]
    indices = []

    for (l, i) in indices_lookup:
        if l is None:
            try:
                ind = [ i for i, x in enumerate(chunks) if x.startswith("What's next for") ][0]
                indices.append((i, ind))
                output['name'] = chunks[ind][16:]
            except IndexError:
                pass
        else:
            try:
                indices.append((i, chunks.index(l)))
            except ValueError:
                pass

    indices = [ (ind, name) for name, ind in indices ]
    indices.sort()
    indices.append((len(chunks), None))

    for i, (ind, name) in enumerate(indices):
        if name is not None:
            ind2 = indices[i+1][0]
        else:
            continue

        chunks_sliced = chunks[ind+1:ind2]

        if name != "built_with":
            chunks_sliced = '\n'.join(chunks_sliced)

        output[name] = chunks_sliced

    for n in indices_names:
        if output.get(n) is None:
            output[n] = None

    return output


def run_model(prefix):
    output_dict = None

    while output_dict is None:
        output_list = [
            "Home page",
            "Inspiration",
            "We are a group of queer people from Montreal who love to shop. We were inspired by our experiences in",
            "Canada's largest city, Calgary, where we were able to shoplift almost as many things as we like. We",
            "wanted to see if this is just an anomaly or if there is a universal way to shoplift. We decided to lo",
            "ok into this after hearing about another person's experience in Calgary. We decided to investigate be",
            "cause we have personal experience of being shoplifted. We want to make things right.",
            "What it does",
            "Our app takes the customer through a series of questions designed to make them feel like they are on",
            "the right track to becoming a better person. At the end of the process, we post their information on",
            "the app to see how it has gone and if it is still relevant.",
            "How I built it",
            "We used Adobe XD to create the icons and the front-end for our app.",
            "Challenges I ran into",
            "We had to make some cuts from the original project to make it usable. We had to make some creative us",
            "e of the camera shake in the app to get the most out of the time we had.",
            "Accomplishments that I'm proud of",
            "What we learned",
            "What's next for Beyond the Market",
            "Built With",
            "adobe-xd",
            "css",
            "flask",
            "geopy",
            "html5",
            "it.",
            "Try it out",
            "GitHub Repo",
            "photobucket.com",
            "",
        ]

        try:
            output_dict = to_dict(output_list)
        except:
            print("Parsing error, trying again")
            pass
    return output_dict

while True:
    print("Waiting for message...")

    try:
        message = json.loads(socket.recv_string())
    except:
        print("Error receiving message :(")
        socket.send_string("")
        continue

    print("Received request: {}".format(message))

    try:
        inspiration = message.get("inspiration")
    except:
        socket.send_string("")
        continue

    prefix = "Inspiration\n{}".format(inspiration)
    try:
        result = run_model(prefix)
    except:
        socket.send_string("")
        continue

    print("Result: {}".format(result))
    try:
        to_send = json.dumps(result)
    except:
        socket.send_string("")
        continue
    socket.send_string(to_send)
