#!/usr/bin/env python3

import zmq
import json

context = zmq.Context()

#  Socket to talk to server
print("Connecting to GPT-2 server...")
socket = context.socket(zmq.REQ)
socket.connect("tcp://172.18.0.3:5555")

inspirations = [
        "79% of mathematics graduates would like to work as a researcher. Luckily, 100% actually end up as hobbyists.  Clearly, a solution is required. Have I got the solution for you: 'Venture: Using Machine Learning to fix Eduction!' By the Blue Gamers!",
        "Have you ever wanted to repair all sorts of crypto miners, with only limited access to GPUs? Have I got the solution for you: 'Synergy: Using Machine Learning to revolutionise Hackathons!' By the Speculatory NFTs!",
        "On the one hand, tomorrows. On the other hand, crypto miners. How can we unite these? Well, you need 'Eclipse: Using Blockchain / Crypto to solve Eduction!' By the Ecological Disasters!",
        "Have you ever wanted to bring forth all sorts of alternative realities, with only limited access to energy? We're here to solve all of your problems with 'Velocity: Using Web3.0 to revolutionise Health!' By the Socially Conscious Bodgers!",
        "On the one hand, futures. On the other hand, hobbyists. How can we unite these? Have I got the thing for you: 'Hydra: Using AI to solve Health!' By the Fungible CEOs!",
]

#  Do 10 requests, waiting each time for a response
for inspiration in inspirations:
    req = dict(inspiration=inspiration)
    print("Sending request {} ...".format(req))
    socket.send_string(json.dumps(req))

    #  Get the reply.
    message = socket.recv_string()
    print("Received reply {}".format(json.loads(message)))
