# Mockathon

All the code for the "Mockathon" project from Hack Cambridge 2022.
Generate infinite hackathon ideas from a simple questionnaire using the GPT-2 language model.
Examples can be found in the `examples` directory.

Training dataset courtesy of [codesmary/Hackathon-Idea-Generator](https://github.com/codesmary/Hackathon-Idea-Generator).

## Setup

To change the number of workers edit the entrypoint under `api` in `docker-compose.yml`.
This will allow the server to handle more requests without falling over.

```
./prepare.sh
docker-compose build
```

## Run

```
docker-compose run
```
