#!/bin/sh

docker run --gpus all -u $(id -u):$(id -g) -v $(pwd)/samples:/samples -v $(pwd)/checkpoint:/checkpoint -v $(pwd)/scripts:/scripts -v $(pwd)/models:/models -v $(pwd)/input:/input -it --rm gpt2
